const expect = require('chai').expect;
const assert = require('assert');
const { Builder, By, Key, until, ThenableWebDriver, WebElement, Browser } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
require('geckodriver');

const BASE_URL = "https://juvenes.fi";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

describe('UI Tests', () => {
    /** @type {ThenableWebDriver} */
    let driver = undefined
    before(async function() {
        this.timeout(20000); // Increase timeout to 20 seconds
        const options = new firefox.Options();
        options.setBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe');
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .setFirefoxOptions(options)
            .build();
        await driver.get(BASE_URL);
        await sleep(5);    
    });
    it('Can find social media links', async () => {
        const socialMedia = await driver.findElement(By.id("social-links-1631708911317-efeb044e-cfc5"));
    });
    it('Can find the search bar', async () => {
        const search = await driver.findElement(By.className("grve-search"));
        
    });
    it('Can search for Pursio', async() => {
        const result = await driver.findElement(By.className("grve-search-textfield")).sendKeys('Pursio', Key.RETURN);

    });
    it('Can get search result', async () => {
        const result = await driver.findElement(By.className("grve-search-textfield")).sendKeys('Pursio', Key.RETURN);
        await driver.wait(until.urlContains('?s'));
        await driver.wait(until.elementLocated(By.css('.grve-title')));
        const titleElement = await driver.findElement(By.css('.grve-title'));
        const titleText = await titleElement.getText();;
        expect(titleText).to.eq("PURSIO");
    });
    it('Can change language', async () => {
        const firstLink = await driver.findElement(By.css('.grve-language > li:first-child > a'));
        await firstLink.click();
        await driver.wait(until.urlContains('en/'));
    })

    after(async function() {
        this.timeout(10000); // Increase timeout to 10 seconds
        await driver.close();
    });
})